"use strict"

const _ = require("lodash")

const Env = use("Env")
const serverURL = Env.get("serverURL")

const Const = use("App/Library/Const")

const Helper = use("App/Library/Helper")
const Common = use("App/Library/Common")
const TextMessenger = use("App/Library/TextMessenger")

const BaseService = use("App/Service/BaseService")

const Route = use("Route")

class MessageService extends BaseService {
    static async messageHandle({event, messenger}) {
        let sender = event.sender.id

        if (event.message.text) {
            try {
                await messenger.sendAction(sender, "typing_on")

                let text = Common.removeSignVietnamese(event.message.text)
                let html = await Helper.requestRasa(text)

                if (html && !_.isNil(html.intent) && !_.isNil(html.intent.name)) {
                    let action = html.intent.name
                    action = action.replace(/[ ]/g, "")
                    action = action.replace(/[:.]/g, "_")

                    switch (action) {
                        case "menu_view": {
                            await messenger.sendTextMessage(sender, "Chọn dịch vụ bạn muốn tìm", Const.quick_reply_common)
                            break
                        }

                        default:
                            let smalltalk = await TextMessenger.processSmallTalk(sender, action)
                            if (smalltalk) {
                                messenger.sendTextMessage(sender, smalltalk, Const.quick_reply_common)
                            }
                            break
                    }
                } else {
                    messenger.sendTextMessage(sender, "Chúng tôi chưa hiểu rõ câu hỏi này 😅", Const.quick_reply_common)
                }
            } catch (e) {
                Logger.error('messageHandle error: ', e)
            }
        }
    }
}

module.exports = MessageService