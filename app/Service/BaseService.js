"use strict"

class BaseService
{
    static async postbackHandle(options = {}) {

    }

    static async attachmentHandle(options = {}) {

    }

    static async messageHandle(options = {}) {

    }

    static async quickReplyHandle(options = {}) {

    }

    static async processAwaitingAction(action, options = {}) {

    }
}

module.exports = BaseService