"use strict"

const Env = use("Env")
const serverURL = Env.get("serverURL")

const _ = require("lodash")

const BaseService = use("App/Service/BaseService")
const UserDAO = use("App/Models/DAO/UserDAO")
const VehicleDAO = use("App/Models/DAO/VehicleDAO")

const User = use("App/Models/Entity/User")
const Vehicle = use("App/Models/Entity/Vehicle")

const CommonDAO = use("App/Models/DAO/CommonDAO")
const Redis = use("Redis")
const Const = use("App/Library/Const")

const key_awaiting_action = "awaitingAction"

class CommonService extends BaseService
{
    static async postbackHandle({ event, messenger }) {
        try {
            let sender = event.sender.id
            let payload = event.postback.payload
            switch (payload) {
                case "get_started": {
                    try {
                        let userId = await this.fetchUser(sender, messenger)
                        if (userId) {
                            const quickReply = [
                                {
                                    content_type: "text",
                                    title: "Thông tin cá nhân",
                                    payload: "user_profile",
                                    image_url: serverURL + "/images/user.png"
                                }
                            ]

                            await messenger.sendTextMessage(sender, "Xin chào, tôi là VP Assistant - trợ lý xe hơi cho bạn.")
                            await messenger.sendAction(sender, "typing_on")
                            await messenger.sendTextMessage(sender, "Vui lòng để lại thông tin để nhận các bản tin mới nhất về thị trường xe hơi hằng ngày", quickReply)
                        }
                    } catch (ex) {
                        Logger.error("get_started", ex)
                    }
                    break
                }

                case "get_menu": {
                    await messenger.sendTextMessage(sender, "Chọn dịch vụ bạn muốn tìm", Const.quick_reply_common)
                    break
                }

                default:
                    break
            }
        } catch (e) {
            Logger.error("CommonService postbackHandle", e)
        }
    }

    static async attachmentHandle({ event, messenger }) {
        let sender = event.sender.id

        try {
            let attachments = event.message.attachments

            for (let i = 0; i < attachments.length; i++) {
                let attachment = attachments[i]
                switch (attachment.type) {
                    case 'image': {
                        await messenger.sendTextMessage(sender, "Vui lòng chọn dịch vụ bạn muốn tìm", Const.quick_reply_common)
                        break
                    }
                    default: {
                        break
                    }
                }
            }
        } catch (e) {
            Logger.error("CommonService attachmentHandle", e)
            await messenger.sendTextMessage(sender, Const.label_error, Const.quick_reply_common)
        }
    }

    static async quickReplyHandle({ event, messenger }) {
        let sender = event.sender.id
        let payload = event.message.quick_reply.payload

        try {
            switch (payload) {
                case "back_to_main_menu": {
                    await messenger.sendTextMessage(sender, "⏪ Quay lại", Const.quick_reply_common)
                    await this.setAwaitingAction(sender, null)
                    break
                }

                default:
                    break
            }
        } catch (e) {
            Logger.error("CommonService quickReplyHandle", e)
            await messenger.sendTextMessage(sender, Const.label_error, Const.quick_reply_common)
        }
    }
    
    static async getAwaitingAction(sender) {
        const _key = CommonDAO.buildCacheKey(key_awaiting_action, { sender: sender })
        return await CommonDAO.getCacheData(_key)
    }

    static async setAwaitingAction(sender, action) {
        const _key = CommonDAO.buildCacheKey(key_awaiting_action, {sender: sender})
        await CommonDAO.setCacheData(_key, action, 600)
    }

    static async addUser(data) {
        let fid = data.id
        let lastName = data.last_name ? data.last_name : ""
        let firstName = data.first_name ? data.first_name : ""
        let link = data.link ? data.link : ""
        let gender = data.gender ? (data.gender === "male" ? 0 : 1) : 0
        let locale = data.locale ? data.locale : "vi_VI"
        let timezone = data.timezone ? data.timezone : ""

        let [user] = await UserDAO.getUser({
            fid: fid
        }, 1, 1)
        if (!user) {
            let userId = await UserDAO.insertUser({
                fid: fid,
                last_name: lastName,
                first_name: firstName,
                lang: locale,
                timezone: timezone,
                gender: gender,
                link: link
            })

            if (userId) {
                try {
                    await UserDAO.insertUserRole({
                        user_id: userId,
                        role_id: User.role.USER
                    })
                } catch (e) {
                    Logger.error("insertUserRole", e)
                }
            }

            return userId
        } else {
            return user.id
        }
    }

    static async fetchUser(sender, messenger) {
        try {
            // Fetch user's public information
            let data = await messenger.getGraphApi(sender)
            Logger.info("fetchUser data", typeof data, data)

            if (data) {
                data = JSON.parse(data)

                let userId = await this.addUser(data)
                //Add this fid to label user
                //await messenger.addUserToLabel(sender, User.label.user)

                if (userId) {
                    if (!_.isNil(data.id)) {
                        //Set to cache to check user is fetched or not
                        UserDAO.setLanguage(data.id, !_.isNil(data.locale) ? data.locale : "vi_VN")
                    }

                    //Insert record of sender's vehicle
                    let [vehicle] = await VehicleDAO.getVehicle({user_id: userId}, 1, 1)
                    if (!_.isNil(vehicle) && !_.isNil(vehicle.id)) {
                        await VehicleDAO.updateVehicle({
                            user_id: userId,
                            id: vehicle.id
                        })
                    } else {
                        await VehicleDAO.insertVehicle({
                            user_id: userId,
                            status: Vehicle.status.ENABLED
                        })
                    }
                }

                return userId
            } else {
                Logger.error("fetchUser NULL", sender)
                return null
            }
        } catch (ex) {
            Logger.error("fetchUser error", sender, ex)
            return null
        }
    }
}

module.exports = CommonService