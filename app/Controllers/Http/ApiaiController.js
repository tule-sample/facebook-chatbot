"use strict"

const _ = require("lodash")

const Env = use("Env")
const serverURL = Env.get("serverURL")
const ACCESS_TOKEN = Env.get("MESSENGER_TOKEN")
const Messenger = use("App/Library/Messenger")
const messenger = new Messenger(ACCESS_TOKEN)
const TextMessenger = use("App/Library/TextMessenger")
const Const = use("App/Library/Const")

const listService = Const.listService

class ApiaiController {
    async post({request, response}) {
        let sender = request.post().sessionId

        try {
            if (!_.isNil(request.post())) {
                let action = request.post().result.action

                if (!_.isNil(sender)) {
                    if (!_.isNil(action)) {
                        Logger.info("processSmallTalk", action)
                        action = action.replace(/[ ]/g, "")
                        action = action.replace(/[:.]/g, "_")
                        Logger.info("ACTION AFTER PROCESS ", action)

                        switch (action) {
                            case "findplace_gas_station": {
                                let item = listService[0]
                                if (item) {
                                    let web_url = serverURL + item.payload
                                    await messenger.sendGenericTemplate(sender, "Định vị " + item.title, item.image_url, "", web_url,
                                        [
                                            {
                                                "type": "web_url",
                                                "url": web_url,
                                                "title": "🔍 Tìm ngay",
                                                "webview_height_ratio": "full",
                                                "messenger_extensions": "true",
                                                "webview_share_button": "hide"
                                            }
                                        ]
                                    )
                                    await messenger.sendTextMessage(sender, "Chọn dịch vụ khác", Const.quick_reply_common)
                                }
                                break
                            }

                            case "findplace_parking": {
                                let item = listService[1]
                                if (item) {
                                    let web_url = serverURL + item.payload
                                    await messenger.sendGenericTemplate(sender, "Định vị " + item.title, item.image_url, "", web_url,
                                        [
                                            {
                                                "type": "web_url",
                                                "url": web_url,
                                                "title": "🔍 Tìm ngay",
                                                "webview_height_ratio": "full",
                                                "messenger_extensions": "true",
                                                "webview_share_button": "hide"
                                            }
                                        ]
                                    )
                                    await messenger.sendTextMessage(sender, "Chọn dịch vụ khác", Const.quick_reply_common)
                                }
                                break
                            }

                            case "findplace_book_store": {
                                let item = listService[2]
                                if (item) {
                                    let web_url = serverURL + item.payload
                                    await messenger.sendGenericTemplate(sender, "Định vị " + item.title, item.image_url, "", web_url,
                                        [
                                            {
                                                "type": "web_url",
                                                "url": web_url,
                                                "title": "🔍 Tìm ngay",
                                                "webview_height_ratio": "full",
                                                "messenger_extensions": "true",
                                                "webview_share_button": "hide"
                                            }
                                        ]
                                    )
                                    await messenger.sendTextMessage(sender, "Chọn dịch vụ khác", Const.quick_reply_common)
                                }
                                break
                            }

                            case "findplace_store": {
                                let item = listService[3]
                                if (item) {
                                    let web_url = serverURL + item.payload
                                    await messenger.sendGenericTemplate(sender, "Định vị " + item.title, item.image_url, "", web_url,
                                        [
                                            {
                                                "type": "web_url",
                                                "url": web_url,
                                                "title": "🔍 Tìm ngay",
                                                "webview_height_ratio": "full",
                                                "messenger_extensions": "true",
                                                "webview_share_button": "hide"
                                            }
                                        ]
                                    )
                                    await messenger.sendTextMessage(sender, "Chọn dịch vụ khác", Const.quick_reply_common)
                                }
                                break
                            }

                            case "findplace_car_wash": {
                                let item = listService[4]
                                if (item) {
                                    let web_url = serverURL + item.payload
                                    await messenger.sendGenericTemplate(sender, "Định vị " + item.title, item.image_url, "", web_url,
                                        [
                                            {
                                                "type": "web_url",
                                                "url": web_url,
                                                "title": "🔍 Tìm ngay",
                                                "webview_height_ratio": "full",
                                                "messenger_extensions": "true",
                                                "webview_share_button": "hide"
                                            }
                                        ]
                                    )
                                    await messenger.sendTextMessage(sender, "Chọn dịch vụ khác", Const.quick_reply_common)
                                }
                                break
                            }

                            case "findplace_car_repair": {
                                let item = listService[5]
                                if (item) {
                                    let web_url = serverURL + item.payload
                                    await messenger.sendGenericTemplate(sender, "Định vị " + item.title, item.image_url, "", web_url,
                                        [
                                            {
                                                "type": "web_url",
                                                "url": web_url,
                                                "title": "🔍 Tìm ngay",
                                                "webview_height_ratio": "full",
                                                "messenger_extensions": "true",
                                                "webview_share_button": "hide"
                                            }
                                        ]
                                    )
                                    await messenger.sendTextMessage(sender, "Chọn dịch vụ khác", Const.quick_reply_common)
                                }
                                break
                            }

                            case "findplace_car_rental": {
                                let item = listService[6]
                                if (item) {
                                    let web_url = serverURL + item.payload
                                    await messenger.sendGenericTemplate(sender, "Định vị " + item.title, item.image_url, "", web_url,
                                        [
                                            {
                                                "type": "web_url",
                                                "url": web_url,
                                                "title": "🔍 Tìm ngay",
                                                "webview_height_ratio": "full",
                                                "messenger_extensions": "true",
                                                "webview_share_button": "hide"
                                            }
                                        ]
                                    )
                                    await messenger.sendTextMessage(sender, "Chọn dịch vụ khác", Const.quick_reply_common)
                                }
                                break
                            }

                            case "findplace_laundry": {
                                let item = listService[7]
                                if (item) {
                                    let web_url = serverURL + item.payload
                                    await messenger.sendGenericTemplate(sender, "Định vị " + item.title, item.image_url, "", web_url,
                                        [
                                            {
                                                "type": "web_url",
                                                "url": web_url,
                                                "title": "🔍 Tìm ngay",
                                                "webview_height_ratio": "full",
                                                "messenger_extensions": "true",
                                                "webview_share_button": "hide"
                                            }
                                        ]
                                    )
                                    await messenger.sendTextMessage(sender, "Chọn dịch vụ khác", Const.quick_reply_common)
                                }
                                break
                            }

                            default:
                                let smalltalk = await TextMessenger.processSmallTalk(sender, action)
                                if (smalltalk) {
                                    messenger.sendTextMessage(sender, smalltalk, Const.quick_reply_common)
                                }
                                break
                        }
                    } else {
                        messenger.sendTextMessage(sender, "Chúng tôi chưa hiểu rõ câu hỏi này 😅", Const.quick_reply_common)
                    }
                }
            }
        } catch (e) {
            Logger.error(e)
            await messenger.sendTextMessage(sender, Const.label_error, Const.quick_reply_common)
        }
    }
}

module.exports = ApiaiController