"use strict"

const Env = use("Env")
const serverURL = Env.get("serverURL")

let dt = new Date()
let day = dt.getDate()
let month = dt.getMonth() + 1
let year = dt.getFullYear()

const filePath = `/uploads/${year}/${month}/${day}/`

class AjaxController {
    async ajax({auth, request, response, session, view}) {
        let action = request.all()
        let data = {
            code: 400,
            message: ''
        }

        if (action.act) {
            try {
                switch (action.act) {
                    case "upload-image": {
                        let file = request.file('file_upload', {
                            types: ['image'],
                            maxSize: '10mb',
                            allowedExtensions: ['jpg', 'png', 'jpeg','JPG']
                        })
                        
                        try {
                            await file.move('public/' + filePath)
                        } catch (e) {
                            //file exist
                            if (e.errno === -17) {
                                return await {
                                    code: 200,
                                    message: serverURL + e.dest.replace('public', '')
                                }
                            } else {
                                throw e
                            }
                        }

                        if (!file.moved()) {
                            return await {
                                code: 400,
                                message: file.error()
                            }
                        } else {
                            return await {
                                code: 200,
                                message: serverURL + filePath + file.fileName
                            }
                        }

                        break
                    }
                    default:
                        break
                }
            } catch (ex) {
                Logger.error("Admin ajax error", ex)
                return await {
                    code: 400,
                    message: ex
                }
            }
        }

        return await data
    }
}

module.exports = AjaxController