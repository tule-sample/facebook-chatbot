"use strict"

const _ = require("lodash")

const Env = use("Env")
const ACCESS_TOKEN = Env.get("MESSENGER_TOKEN")
const BOT_FID = Env.get("BOT_FID")
const apiaiApp = require("apiai")(Env.get("APIAI_CLIENT_TOKEN"))
const validationToken = Env.get("validationToken")

const CommonService = use("App/Service/CommonService")
const MessageService = use("App/Service/MessageService")

const UserDAO = use("App/Models/DAO/UserDAO")

const Messenger = use("App/Library/Messenger")
const messenger = new Messenger(ACCESS_TOKEN)

const Const = use("App/Library/Const")

class WebhookController {
    async get({request, response}) {
        if (request.get()["hub.verify_token"] === validationToken) {
            response.status(200).send(request.get()["hub.challenge"])
        }
        else {
            response.send("Error, wrong token")
        }
    }

    async post({request, response}) {
        if (request.body.object === "page") {
            if (request.body.entry) {
                request.body.entry.forEach(async (entry) => {
                    if (entry.messaging) {
                        entry.messaging.forEach(async (event) => {
                            //Not process what bot echo back
                            if (!_.isNil(event.sender) && !_.isNil(event.sender.id) && event.sender.id !== BOT_FID) {
                                //Check fetch user
                                await this.checkFetchUser(event, messenger)

                                if (event.postback) {
                                    this.postbackHandle(event)
                                } else if (event.referral) {
                                    this.referralHandle(event)
                                } else if (event.message) {
                                    //Handle Awaiting action
                                    let awaitingAction = await CommonService.getAwaitingAction(event.sender.id)
                                    if (awaitingAction) {
                                        this.processAwaitingAction(awaitingAction, event)
                                    }
                                    else if (event.message.attachments !== undefined) {
                                        //Handle Attachment action
                                        this.attachmentHandle(event)
                                    }
                                    else if (event.message.text !== undefined) {
                                        //Handle Quick Reply & Text message action
                                        event.message.quick_reply !== undefined ? this.quickReplyHandle(event) : this.messageHandle(event)
                                    }
                                }
                            }
                        })
                    }
                })
                response.status(200).end()
            }
        }
    }

    async checkFetchUser(event, messenger) {
        let sender = event.sender.id
        let lang = await UserDAO.getLanguage(sender)

        if (_.isNil(lang) || _.isNull(lang) || _.isEmpty(lang)) {
            Logger.warning("Fetch user again", sender)
            await CommonService.fetchUser(sender, messenger)
        }
    }

    async postbackHandle(event) {
        try {
            if (await CommonService.getAwaitingAction(event.sender.id)) {
                //Delete all awaiting action in cache
                CommonService.setAwaitingAction(event.sender.id)
            }
        } catch (e) {
            Logger.error("Error reset cache", e)
        }

        CommonService.postbackHandle({ event, messenger })
    }

    async attachmentHandle(event) {
    }

    async quickReplyHandle(event) {
        //Common service
        CommonService.quickReplyHandle({ event, messenger })
    }

    async processAwaitingAction(awaitingAction, event) {
    }

    async messageHandle(event) {
        MessageService.messageHandle({ event, messenger })
    }

    async referralHandle(event) {
        let sender = event.sender.id

        try {
            let ref = event.referral.ref
            Logger.info('REFERAL ', sender, ref)

            if (ref) {
                switch (ref) {
                    case 'user':
                        break
                    default:
                        break
                }
            }
        } catch (e) {
            Logger.error("referralHandle error", e)
            await messenger.sendTextMessage(sender, Const.label_error, Const.quick_reply_common)
        }
    }
}

module.exports = WebhookController