"use strict"

const _ = require("lodash")

const Redis = use("Redis")

const BaseModel = use("App/Models/BaseModel")
const User = use("App/Models/Entity/User")
const Database = use("Database")

class UserDAO extends BaseModel
{
    static async insertUser(item = {}) {
        let query = new User

        item.created_at = new Date()
        item.updated_at = new Date()

        this.resetCache(BaseModel.CacheType.MODEL_USER)

        return await query.insert(item)
    }

    static async insertUserRole(item = {}) {
        return await Database.table("user_role_rel").insert(item)
    }

    static async updateUser(item = {}) {
        let query = new User

        query.update("updated_at", new Date())

        if (!_.isNil(item.email)) {
            query.update("email", item.email)
        }

        if (!_.isNil(item.phone)) {
            query.update("phone", item.phone)
        }

        if (!_.isNil(item.first_name)) {
            query.update("first_name", item.first_name)
        }

        if (!_.isNil(item.last_name)) {
            query.update("last_name", item.last_name)
        }

        if (!_.isNil(item.status)) {
            query.update("status", item.status)
        }

        if (!_.isNil(item.gender)) {
            query.update("gender", item.gender)
        }

        if (!_.isNil(item.lang)) {
            query.update("lang", item.lang)
        }

        if (!_.isNil(item.timezone)) {
            query.update("timezone", item.timezone)
        }

        if (!_.isNil(item.link)) {
            query.update("link", item.link)
        }

        if (!_.isNil(item.name)) {
            query.update("name", item.name)
        }

        if (!_.isNil(item.address)) {
            query.update("address", item.address)
        }

        if (!_.isNil(item.birthdate)) {
            query.update("birthdate", item.birthdate)
        }

        if (!_.isNil(item.province_id)) {
            query.update("province_id", item.province_id)
        }

        !_.isNil(item.fid) ? query.where("fid", item.fid) : query.where("id", item.id)

        this.resetCache(BaseModel.CacheType.MODEL_USER)

        return await query
    }

    static async getUser(options = {}, pageIndex = 1, pageSize = 10) {
        const _key = this.buildCacheKey("getUser", arguments)
        let _cData = await this.getCacheData(_key)
        if (_cData) {
            return _cData
        }

        let obj = new User
        let query = !_.isNil(options.field) ? obj.select(options.field) : obj.select("*")

        if (!_.isNil(options.id)) {
            query.where("id", options.id)
        }

        if (!_.isNil(options.ids)) {
            query.whereIn("ids", options.ids)
        }

        if (!_.isNil(options.fid)) {
            query.where("fid", options.fid)
        }

        if (!_.isNil(options.fuid)) {
            query.where("fuid", options.fuid)
        }

        if (!_.isNil(options.email)) {
            query.where("email", options.email)
        }

        if (!_.isNil(options.phone)) {
            query.where("phone", options.phone)
        }

        if (!_.isNil(options.gender)) {
            query.where("gender", options.gender)
        }

        if (!_.isNil(options.status)) {
            query.where("status", options.status)
        }

        if (!_.isNil(options.province_id)) {
            query.where("province_id", options.province_id)
        }

        if (!_.isNil(options.search) && options.search.length > 0) {
            query.where("email", "LIKE", "%" + options.search + "%")
            query.orWhere("phone", "LIKE", "%" + options.search + "%")
        }

        query.orderBy("created_at", "desc")
        query.forPage(pageIndex, pageSize)

        let records = await query
        let result = []

        if (records.length > 0) {
            for (let item of records) {
                result.push({
                    id: item.id,
                    fid: item.fid,
                    fuid: item.fuid,
                    email: item.email,
                    first_name: item.first_name,
                    last_name: item.last_name,
                    profile_pic: item.profile_pic,
                    lang: item.lang,
                    gender: item.gender,
                    status: item.status,
                    phone: item.phone,
                    timezone: item.timezone,
                    link: item.link,
                    name: item.name,
                    address: item.address,
                    birthdate: new Date(item.birthdate).toLocaleString(),
                    broadcast_register: item.broadcast_register,
                    province_id: item.province_id,
                    created_at: new Date(item.created_at).toLocaleString(),
                    updated_at: new Date(item.updated_at).toLocaleString(),
                })
            }
        }

        await this.setCacheData(_key, result)

        return await result
    }

    static async setPhoneNumber(sender, phone) {
        return await this.updateUser({
            phone,
            fid: sender
        })
    }

    static async setEmail(sender, email) {
        return await this.updateUser({
            email,
            fid: sender
        })
    }

    static async setLanguage(fid, lang) {
        return await Redis.hset(fid, "language", lang)
    }

    static async getLanguage(fid) {
        return await Redis.hget(fid, "language")
    }
}

module.exports = UserDAO