"use strict"

const Model = use("Model")
const Redis = use("Redis")
const Logger = use("Logger")
const Promise = require("bluebird")
const _ = require("lodash")

const prefix = "chatbot"
let cacheActive = true

class BaseModel extends Model {
    constructor(props) {
        super(props)
    }

    static buildCacheKey(fnName, args) {
        return `${this.name}.${fnName}.${JSON.stringify(args)}`
    }

    static async setCacheData(key, data, ttl = 20000) {
        key = prefix + "." + key
        if (!data){
            await Redis.del(key)
        }
        else{
            if (ttl) {
                await Redis.set(key, JSON.stringify(data))
            } else {
                await Redis.setex(key, ttl / 1000, JSON.stringify(data))
            }
        }
    }

    static async getCacheData(key) {
        if (!cacheActive)
            return null
        key = prefix + "." + key
        let result = await Redis.get(key)

        if (!result)
            return null

        try {
            const obj = JSON.parse(result)
            if (obj.__meta__) obj.$sideLoaded = obj.__meta__
            return obj
        } catch (ignored) {
            return result
        }
    }

    static setCacheActive(active = true) {
        cacheActive = active
    }

    static async resetCache(type, ...filters) {
        const MAX_CACHES_ON_RESET = 150

        let filterTexts = []
        if (filters) {
            filters.forEach(filter => {
                switch (typeof filter) {
                    case "string":
                        filterTexts.push(filter)
                        break
                    case "number":
                        filterTexts.push(filter)
                        break
                    case "object":
                        filterTexts.push(JSON.stringify(filter).slice(1, -1))
                        break
                }
            })
        }

        let functionsList
        switch (type) {
            case BaseModel.CacheType.MODEL_USER:
                functionsList = [
                    "UserDAO.getUser",
                    "User.getUser",
                ]
                break
            case BaseModel.CacheType.AWAITING_ACTION:
                functionsList = [
                    "CommonService.awaitingAction",
                ]
                Logger.info("functionsList", functionsList)
                break
            default:
                break
        }

        if (functionsList && functionsList.length) {
            // Tìm và invoke hết lại đám hàm cache
            await Promise.map(functionsList, async (func, index) => {
                // 1. Tìm cache tương ứng với hàm
                const startsWith = `${prefix}.${func}.`
                let cacheKeys
                if (filterTexts.length) {
                    const data = await Promise.map(filterTexts, async filterText => {
                        return await Redis.keys(`${startsWith}*${filterText}*`)
                    })
                    cacheKeys = []
                    data.forEach(p => {
                        cacheKeys.push(...p)
                    })
                    cacheKeys = _.uniq(cacheKeys)
                } else {
                    cacheKeys = await Redis.keys(`${startsWith}*`)
                }
                Logger.notice("CACHE keys to reinvoke, index=" + index, "length=" + cacheKeys.length)

                // 2. Giữ lại N thằng đầu trong list, xóa hết những thằng khác
                const toDeleteKeys = cacheKeys.splice(MAX_CACHES_ON_RESET)

                // 3. Xóa + re-invoke
                async function reinvoke(cacheKey) {
                    try {
                        Logger.debug("CACHE reinvoke", cacheKey)
                        const args = JSON.parse(cacheKey.substring(startsWith.length))
                        const [model, fnName] = func.split(".")

                        try {
                            const M = use(`App/Models/DAO/${model}`)
                            M.setCacheActive(false)
                            await M[fnName](..._.map(args, arg => arg)) // Convert args to array and re-invoke
                            M.setCacheActive(true)
                        } catch (e) {

                        }
                    } catch (err) {
                        Logger.warning("CACHE REINVOKE ERROR", err)
                        await Redis.del(cacheKey)
                    }
                }

                await Promise.all([
                    toDeleteKeys.length ? Redis.del(toDeleteKeys) : Promise.resolve(),
                    Promise.map(cacheKeys, reinvoke)
                ])
            })
        }
    }
}

module.exports = BaseModel

BaseModel.CacheType = {
    MODEL_USER: 'MODEL_USER',
    AWAITING_ACTION: 'AWAITING_ACTION'
}