"use strict"

const Model = use("Model")
const Database = use("Database")

class User extends Model {
    get table() {
        return "user"
    }

    constructor(props) {
        super(props);
        return Database.table(this.table)
    }
}

module.exports = User

User.status = {
    DISABLED: 0,
    ENABLED: 1
}

User.gender = {
    MALE: 0,
    FEMALE: 1
}

User.role = {
    ADMIN: 1,
    VERIFIER: 2,
    USER: 3
}

User.label = {
    user: 1622511364451519
}

User.province_id = {
    TYPE_1: 1,
    TYPE_2: 2,
}

User.list_province = [
    {
        id: User.province_id.TYPE_1,
        name: "Hà Nội"
    },
    {
        id: User.province_id.TYPE_2,
        name: "Hồ Chí Minh"
    },
]