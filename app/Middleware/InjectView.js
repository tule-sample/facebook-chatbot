'use strict'

const moment = require('moment')
const View = use('View')

class InjectView {
    async handle({request}, next) {
        View.global('moment', moment)
        //Define no image url on view
        View.global('no_image_url', "https://vpb.hellonami.com/images/no_images.jpg")
        // call next to advance the request
        await next()
    }
}

module.exports = InjectView