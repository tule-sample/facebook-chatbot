const request_promise = require('request-promise')
const request = require('sync-request')
const jwt = require('jsonwebtoken')
const _ = require("lodash")

const Env = use("Env")

class Helper {
    static async encodeId(fid) {
        return jwt.sign({ fid: fid }, Env.get('APP_KEY'))
    }

    static async decodeId(id) {
        try {
            let result = jwt.verify(id, Env.get('APP_KEY'))
            return !_.isNil(result.fid) ? result.fid : 0
        } catch (e) {
            console.log("DecodeId exception", e)
            return 0
        }
    }

    static async requestRasa(text) {
        return request_promise({
            uri: Env.get('RASA_ENDPOINT'),
            qs: {
                q: text
            },
            method: 'GET',
            json: true
        }).catch(error => {
            Logger.error('SEND MESSAGE ERROR: ', error.message)
        })
    }

    static getHtml(url, method = "GET", options = {}) {
        let res = request(method, url, options)
        return res.getBody('utf8')
    }

    static getDomain(url) {
        let domain = null;
        let match = /^(?:https?:\/\/)?([^\/]+)/i.exec(url)
        if (match.length > 0) {
            domain = match[1]
        }
        return domain;
    }
    /* End getDomain */

    static getDomainUrl(url) {
        let domain = null;
        let match = /^(https?:\/\/)([^\/?]+)/i.exec(url)
        if (match.length > 0) {
            domain = match[1] + match[2]
        }
        return domain
    }

    static repairUrl(url, domain) {
        url = url.trim('/');
        if (/^\/\//.test(url)) {
            return 'http:' + url;
        } else if (/^\/?\.+\//.test(url)) {
            return domain + url.replace(/\/?(\.+\/)+/is, '/', url);
        } else if (/^\//.test(url)) {
            return domain + url;
        } else if (/^http/.test(url)) {
            return url;
        } else if (/^[a-z]/.test(url)) {
            return null;
        }
        return url;
    }
}

module.exports = Helper