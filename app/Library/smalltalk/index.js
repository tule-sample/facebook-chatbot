'use strict'

let vi = require('./vi');
let en = require('./en');

module.exports = function (lang) {
    return (lang && lang.startsWith('en')) ? en : vi
};