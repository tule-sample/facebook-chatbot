'use strict'

const smalltalk = use("App/Library/smalltalk/index")

class TextMessenger {
    static async processSmallTalk(sender, action, language = "vi") {
        if (sender.indexOf("-") > -1) {
            Logger.error("HANDLE TEST API.AI: ", action)
        } else {
            if (action !== "input_unknown") {
                try {
                    let listTalk = smalltalk(language)[action]

                    if (listTalk && listTalk.length > 0) {
                        return listTalk[Math.floor(Math.random() * listTalk.length)]
                    }
                }
                catch (err) {
                    return "Xin lỗi, chúng tôi chưa hiểu yêu cầu của bạn 😅"
                }
            }
        }

        return "Xin lỗi, chúng tôi chưa hiểu yêu cầu của bạn 😅"
    }
}

module.exports = TextMessenger