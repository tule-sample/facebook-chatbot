'use strict'

const Env = use("Env")
const serverURL = Env.get("serverURL")

const Route = use("Route")

class Const { }

module.exports = Const

Const.quick_reply_common = [
    {
        content_type: "text",
        title: "Thông tin tài khoản",
        payload: "menu_profile",
        image_url: serverURL + "/images/user.png"
    },
]

Const.label_error = "Có lỗi xảy ra, vui lòng chọn lại dịch vụ"