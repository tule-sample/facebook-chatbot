const request = require('request-promise');
const _ = require("lodash")

class Messenger {
    constructor(token) {
        this.token = token
    }

    async sendMessageToFb(data) {
        return request({
            uri: 'https://graph.facebook.com/v3.0/me/messages',
            qs: {
                access_token: this.token
            },
            method: 'POST',
            json: data
        }).catch(error => {
            Logger.error('SEND MESSAGE ERROR: ', error.message)
        })
    }

    async setMessengerProfile(data) {
        return request({
            uri: 'https://graph.facebook.com/v3.0/me/messenger_profile?access_token',
            qs: {
                access_token: this.token
            },
            method: 'POST',
            json: data
        }).catch(error => {
            Logger.error('Profile API Error: ', error)
        })
    }

    async whitelistDomain([url]) {
        let data = {
            whitelisted_domains: url
        };
        return this.setMessengerProfile(data)
    }

    async sendListMessage(psid, elements, quickReply) {
        let data = {
            recipient: {
                id: psid
            },
            message: {
                attachment: {
                    type: 'template',
                    payload: {
                        template_type: 'list',
                        elements: elements,
                    }
                },
                quick_replies: quickReply
            }
        };
        return this.sendMessageToFb(data)
    }

    //works with url, postback, call,log in, log out button
    async sendButton(psid, text, button, quickReply) {
        let data = {
            "recipient": {
                "id": psid
            },
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "button",
                        "text": text,
                        "buttons": button
                    }
                },
                "quick_replies": quickReply

            }
        };
        return this.sendMessageToFb(data)
    }

    async sendTextMessage(psid, text, quickReply) {
        let data = {
            recipient: {
                id: psid
            },
            message: {
                text: text,
                quick_replies: quickReply
            }
        };
        return this.sendMessageToFb(data)
    }

    async sendAction(psid, action = 'typing_on') {
        let data = {
            "recipient": {
                "id": psid
            },
            "sender_action": action //mark_seen,typing_on,typing_off
        }
        return this.sendMessageToFb(data)
    }

    async getGraphApi(psid) {
        return request({
            uri: `https://graph.facebook.com/v3.0/${psid}`,
            qs: {
                //fields: "id, last_name, first_name, link, gender, locale, verified, timezone, email",
                access_token: this.token
            },
            method: 'GET',
        }).catch(error => {
            Logger.error('GRAPH API ERROR: ', error.message)
        })
    }

    async setPersistentMenu(menu) {
        return this.setMessengerProfile(menu)
    }

    async sendGenericTemplate(psid, title, image_url, subtitle, web_url, button) {
        let data = {
            "recipient": {
                "id": psid
            },
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": [
                            {
                                "title": title,
                                "image_url": image_url,
                                "subtitle": subtitle,
                                "default_action": {
                                    "type": "web_url",
                                    "url": web_url,
                                    "messenger_extensions": false,
                                    "webview_height_ratio": "tall"
                                },
                                "buttons": button
                            }
                        ]
                    }
                }
            }
        }
        return this.sendMessageToFb(data)
    }

    async sendListGenericTemplate(psid, listItem) {
        let listElement = []
        for (let item of listItem) {
            listElement.push({
                "title": !_.isNil(item.title) ? item.title : "",
                "image_url": !_.isNil(item.image_url) ? item.image_url : "",
                "subtitle": !_.isNil(item.subtitle) ? item.subtitle : "",
                "default_action": {
                    "type": "web_url",
                    "url": !_.isNil(item.web_url) ? item.web_url : "",
                    "messenger_extensions": false,
                    "webview_height_ratio": "tall"
                },
                "buttons": !_.isNil(item.buttons) ? item.buttons : [
                    {
                        "type": "web_url",
                        "url": !_.isNil(item.web_url) ? item.web_url : "",
                        "title": "🔍 Xem chi tiết",
                        "webview_height_ratio": "full",
                        "messenger_extensions": "true",
                        "webview_share_button": "hide"
                    }
                ],
            })
        }

        let data = {
            "recipient": {
                "id": psid
            },
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": listElement
                    }
                }
            }
        }
        return this.sendMessageToFb(data)
    }

    //===Broad cast message

    //On success, the Broadcast API will return an object with a message_creative_id property that can be used to send the message:
    async createBroadcastMessage(messageContent) {
        return request({
            uri: 'https://graph.facebook.com/v3.0/me/message_creatives',
            qs: {
                access_token: this.token
            },
            method: 'POST',
            json: {
                messages:[messageContent]
            }
        }).catch(error => {
            Logger.error('CREATE BROADCAST MESSAGE ERROR: ', error.message)
        })
    }

    async estimateBroadcastMessage(broadcastID) {
        return setTimeout(function () {
            return request({
                uri: `https://graph.facebook.com/v3.0/${broadcastID}/insights/messages_sent`,
                qs: {
                    access_token: this.token
                },
                method: 'GET'
            }).catch(error => {
                Logger.error('ESTIMATE BROADCAST ERROR: ', error.message)
            })
        },5000)
    }

    //On success, the Messenger Platform will return an id property in the request.
    async createLabel(labelName){
        return request({
            uri: 'https://graph.facebook.com/v3.0/me/custom_labels',
            qs: {
                access_token: this.token
            },
            method: 'POST',
            json: {
                name:labelName
            }
        }).catch(error => {
            Logger.error('CREATE LABEL ERROR: ', error.message)
        })
    }

    //Associating a Label to a PSID
    async addUserToLabel(psid,labelID){
        return request({
            uri: `https://graph.facebook.com/v3.0/${labelID}/label`,
            qs: {
                access_token: this.token
            },
            method: 'POST',
            json: {
                user:psid
            }
        }).catch(error => {
            Logger.error('ADD USER TO LABEL ERROR: ', error.message)
        })
    }

    //Sending a Message with a Label (target broadcast)
    //On success, the Messenger Platform will return a broadcast_id:
    async sendTargetMessage(broadcastMessageID,labelID){
        return request({
            uri: 'https://graph.facebook.com/v3.0/me/broadcast_messages',
            qs: {
                access_token: this.token
            },
            method: 'POST',
            json: {
                message_creative_id:broadcastMessageID,
                custom_label_id:labelID
            }
        }).catch(error => {
            Logger.error('SEND TARGET MESSAGE ERROR: ', error.message)
        })
    }

    // Removing a Label From a PSID
    //On success, the Messenger Platform will return a success message:
    // {
    //     "success": true
    // }
    async removeUserFromLabel(labedID,fid) {
        return request({
            uri: `https://graph.facebook.com/v3.0/${labelID}/label`,
            qs: {
                access_token: this.token
            },
            method: 'DELETE',
            json: {
                user:fid
            }
        }).catch(error => {
            Logger.error('REMOVE USER FROM LABEL ERROR: ', error.message)
        })
    }

    //Retrieving a List of All Labels
    async getAllLabels({ session }) {
        let labels
        if (session) {
            labels = await session.get('labels')
        }
        
        if (labels) return labels
        else {
            labels = await request({
                uri: `https://graph.facebook.com/v3.0/me/custom_labels`,
                qs: {
                    fields: "name",
                    access_token: this.token
                },
                method: 'GET'
            }).catch(error => {
                Logger.error('GET ALL LABEL ERROR: ', error.message)
            })
            if (session) session.put('labels', labels)

            return labels
        }
    }

    //Retrieving a Label
    async getLabel(labelName) {
        let labels = await this.getAllLabels({})
        let labelsArr = JSON.parse(labels).data
        let labelObj
        for(let i = 0; i <= labelsArr.length; i++) {
            if (labelsArr[i].name === labelName) {
                labelObj = labelsArr[i]
                break
            }
        }
        return labelObj
    }
}

module.exports = Messenger;
