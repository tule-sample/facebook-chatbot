"use strict"

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL"s and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use("Route");

Route.get("/", "HomeController.index")

Route.get("/messenger/webhook", "WebhookController.get")
Route.post("/messenger/webhook", "WebhookController.post")
Route.post("/apiai/webhook", "ApiaiController.post")

Route.route("ajax", "AjaxController.ajax", ["GET", "POST"]).as("ajax")