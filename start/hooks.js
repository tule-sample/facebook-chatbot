const {hooks} = require('@adonisjs/ignitor')

hooks.after.providersBooted(() => {
    global.Logger = use('Logger')
})