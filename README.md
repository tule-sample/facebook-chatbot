# facebook-chatbot

This is the fullstack boilerplate for AdonisJs, it comes pre-configured with.

AdonisJs is a Node.js MVC framework that runs on all major operating systems. It offers a stable ecosystem to write server-side web applications so you can focus on business needs over finalizing which package to choose or not.

1. Bodyparser
2. Session
3. Authentication
4. Web security middleware
5. CORS
6. Edge template engine
7. Lucid ORM
8. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```

Make sure to read through our installation guide here: https://adonisjs.com/docs/4.1/installation.

Checkout facebook messenger platform here: https://developers.facebook.com/docs/messenger-platform/